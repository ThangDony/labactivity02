package labactivity02.code.hellopackage;
import labactivity02.code.secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;

public class Greeter {

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a value: ");
        int numberInput = scan.nextInt();

        int doubledValue = Utilities.doubleMe(numberInput);

        System.out.println("Doubled value: " + doubledValue);
    }
    
}
